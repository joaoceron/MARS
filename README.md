<h2>MARS Repository</h2>
This repository contains resources used in the development of <b>MARS: An SDN-Based Malware Analysis Solution</b>. <br>
Next, we show the list of analyzed malwares and, in sequence, the respective reports provided by the sandbox <br>
running in MARS infrastructure.